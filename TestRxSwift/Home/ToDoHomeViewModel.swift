//
//  ViewModel.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/4/15.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class ToDoHomeViewModel {
    
    private let bag = DisposeBag()
    private let toDoDataManager: TxtDataManager<ToDoItem>
    
    // AnimationSectionedDataSoure Test
    let todoItems: BehaviorRelay<[AnimatableSectionModel<String, ToDoItem>]> = BehaviorRelay(value: [])
    
//    let todoItems: BehaviorRelay<[SectionModel<String, ToDoItem>]> = BehaviorRelay(value: [])
    
    let syncSuccess: PublishSubject<()> = PublishSubject()
    
    let errorMsg: PublishSubject<String> = PublishSubject()
    
    private let utility: Utility = Utility()
    
    init(toDoDataManager: TxtDataManager<ToDoItem>) {
        self.toDoDataManager = toDoDataManager
        bind()
    }
    
    private func bind() {
        
        toDoDataManager.toDoItems
            .asObservable()
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[unowned self] items in
                // AnimationSectionedDataSoure Test
                let sectionOneModel = AnimatableSectionModel(model: "Undo", items: self.todoList(from: items))
                let sectionTwoModel = AnimatableSectionModel(model: "Completed", items: self.completedList(from: items))
                
//                let sectionOneModel = SectionModel(model: "Undo", items: self.todoList(from: items))
//                let sectionTwoModel = SectionModel(model: "Completed", items: self.completedList(from: items))
                
                self.todoItems.accept([sectionOneModel, sectionTwoModel])
                self.syncSuccess.onNext(())
            })
            .disposed(by: self.bag)
    }
    
    private func todoList(from items: [ToDoItem]) -> [ToDoItem] {
        
        let todoList = items.filter { !$0.isCompletion }
                            .sorted {
                                utility.compareDate(start: $0.startDate, end: $1.startDate, dateFormat: "yyyy/MM/dd") == .orderedAscending
                            }
        
        return todoList
    }
    
    private func completedList(from items: [ToDoItem]) -> [ToDoItem] {
        
        let completedList = items.filter { $0.isCompletion }
                                 .sorted {
                                    utility.compareDate(start: $0.dueDate, end: $1.dueDate, dateFormat: "yyyy/MM/dd") == .orderedAscending
                                  }
        
        return completedList
    }
    
    // MARK: Operate Data
    // Get All
    func retriveTodoList() {

        toDoDataManager.retriveToDoItems()
            .observeOn(MainScheduler.instance)
            .subscribe(onError: { error in
                self.errorMsg.onNext("資料抓取失敗")
            })
            .disposed(by: self.bag)
    }
    
    func add(item: ToDoItem) {
        
        let items = toDoDataManager.toDoItems.value + [item]
        sync(items: items)
    }
    
    private func sync(items: [ToDoItem]) {
        
        toDoDataManager.synchornized(items)
            .observeOn(MainScheduler.instance)
            .subscribe(onError: { error in
                self.errorMsg.onNext("處理失敗")
            })
            .disposed(by: self.bag)
    }
    
    func changeInformation(in items: [ToDoItem]) {
        
        var oldItems = toDoDataManager.toDoItems.value
        
        for item in items {
            let arrayIndex = oldItems.firstIndex { (i) -> Bool in
                return item.identity == i.identity
            }
            
            if let index = arrayIndex {
                oldItems[index] = item
            }
        }
        
        sync(items: oldItems)
    }
    
    func removeItem(at indexPath: IndexPath){

        let item = todoItems.value[indexPath.section].items[indexPath.row]
        
        let arrayIndex = toDoDataManager.toDoItems.value.firstIndex { (i) -> Bool in
            return item.identity == i.identity
        }
        guard let index = arrayIndex else { return }
        
        var list = self.toDoDataManager.toDoItems.value
        list.remove(at: Int(index))
        self.sync(items: list)
    }
}
