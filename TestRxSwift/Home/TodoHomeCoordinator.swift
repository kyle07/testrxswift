//
//  TodoHomeCoordinator.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/9.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit

class TodoHomeCoordinator: Coordinator<UINavigationController>, CoordinatingDependency {
    var dependency: AppDependency?

    private var vc: TodoHomeViewController!
    
    override func start() {
        
        super.start()
        
        guard let dependency = dependency else {
            return
        }
        
        let vm = ToDoHomeViewModel(toDoDataManager: dependency.todoDataManager)
        vc = TodoHomeViewController(vm: vm)
        vc.delegate = self
        rootViewController.viewControllers = [vc]
    }
}

// MARK: Sub-coordinator delegate
extension TodoHomeCoordinator: ItemDetailCoordinatorDelegate {
    
    func modify(item: ToDoItem, from coordinator: ItemDetailCoordinator) {
        vc.modify(item)
    }
    
    func add(item: ToDoItem, from coordinator: ItemDetailCoordinator) {
        vc.add(item)
        rootViewController.popViewController(animated: true)
    }
}

// MARK: ViewController delegate
extension TodoHomeCoordinator: TodoHomeViewControllerDelegate {
    
    func addItem(from: TodoHomeViewController) {
        let nextCoordinator = ItemDetailCoordinator(viewController: self.rootViewController, type: .add)
        nextCoordinator.delegate = self
        nextCoordinator.start()
    }
    
    func modify(item: ToDoItem, from: TodoHomeViewController) {
        
        let nextCoordinator = ItemDetailCoordinator(viewController: self.rootViewController, item: item, type: .modify)
        nextCoordinator.delegate = self
        nextCoordinator.start()
    }
}
