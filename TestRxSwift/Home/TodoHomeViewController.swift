//
//  ViewController.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/4/15.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources

protocol TodoHomeViewControllerDelegate: class {
    func addItem(from: TodoHomeViewController)
    func modify(item: ToDoItem, from: TodoHomeViewController)
}

class TodoHomeViewController: UIViewController {

    weak var delegate: TodoHomeViewControllerDelegate?
    
    fileprivate let vm: ToDoHomeViewModel
    fileprivate var changedInformationItems: [ToDoItem] = []
    
    private let tableViewDisplay: UITableView = {
        
        let tableView = UITableView(frame: .zero, style: .grouped)
        return tableView
    }()
    
    private let disposeBag = DisposeBag()
    
    init(vm: ToDoHomeViewModel) {
        self.vm = vm
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        initNavigationBar()
        initTableView()
        configureLayout()
        
        vm.retriveTodoList()
        
        vm.syncSuccess
            .asObservable()
            .subscribe { [unowned self] in
                self.changedInformationItems.removeAll()
            }
            .disposed(by: disposeBag)
        
        vm.errorMsg
            .asObservable()
            .subscribe(onNext: {[unowned self] in
                self.showAlert(message: $0)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Set Item
    func modify(_ item: ToDoItem) {
        vm.changeInformation(in: [item])
    }
    
    func add(_ item: ToDoItem) {
        vm.add(item: item)
    }
    
    // MARK: Init subviews
    private func initNavigationBar() {
        
        self.navigationItem.title = "ToDo"
        
        navigationItem.rightBarButtonItem = rightBarButtonItem()
        _ = leftBarButtonItem()
    }
    
    private func rightBarButtonItem() -> UIBarButtonItem {
        
        let rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
        
        rightBarButtonItem.rx.tap
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                
                self.delegate?.addItem(from: self)
            })
            .disposed(by: disposeBag)
        
        return rightBarButtonItem
    }
    
    private func leftBarButtonItem() -> UIBarButtonItem {
        
        var leftBarButtonItem: UIBarButtonItem!
        
        if tableViewDisplay.isEditing {
            leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
        }
        else {
            leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: nil, action: nil)
        }
        
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        leftBarButtonItem.rx.tap
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                self.syncItems()
                self.tableViewDisplay.isEditing = !self.tableViewDisplay.isEditing
                self.navigationItem.leftBarButtonItem = self.leftBarButtonItem()
            })
            .disposed(by: disposeBag)
        
        return leftBarButtonItem
    }
    
    private func syncItems() {
        if tableViewDisplay.isEditing == false || changedInformationItems.isEmpty {
            return
        }
        
        vm.changeInformation(in: changedInformationItems)
    }
    
    private func initTableView() {
        tableViewDisplay.rowHeight = 50
        tableViewDisplay.register(ToDoItemTableViewCell.self, forCellReuseIdentifier: ToDoItemTableViewCell.reuseIdentifier)
        view.addSubview(tableViewDisplay)
        
        vm.todoItems
            .bind(to: tableViewDisplay.rx.items(dataSource: configureAnimationedDataSource())) // AnimationSectionedDataSource Test
//            .bind(to: tableViewDisplay.rx.items(dataSource: configureReloadDataSource()))
            .disposed(by: disposeBag)
        
        tableViewDisplay.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
    }
    
    private func configureLayout() {
        
        tableViewDisplay.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-8)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right)
        }
    }
    
    private func showAlert(message: String) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "確定", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension TodoHomeViewController {
    
    // Basic DataSource
    private func configureReloadDataSource() -> RxTableViewSectionedReloadDataSource<SectionModel<String, ToDoItem>> {
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ToDoItem>> (configureCell: { [unowned self] (dataSource, tableView, indexPath, element) -> UITableViewCell in
            
            return self.cell(in: tableView, at: indexPath,info: element)
            
            }, titleForHeaderInSection: { (dataSource, index) in
                
                dataSource.sectionModels[index].model
        })
        
        dataSource.canEditRowAtIndexPath = { dataSource, indexPath in
            return true
        }
        
        return dataSource
    }
    
    private func configureAnimationedDataSource() -> RxTableViewSectionedAnimatedDataSource<AnimatableSectionModel<String, ToDoItem>> {
        
        let dataSource = RxTableViewSectionedAnimatedDataSource<AnimatableSectionModel<String, ToDoItem>> (configureCell: { [unowned self] (dataSource, tableView, indexPath, element) -> UITableViewCell in
            // Memo: 當有移動產生時
            
            return self.cell(in: tableView, at: indexPath,info: element)
            
            }, titleForHeaderInSection: { (dataSource, index) in
                
                dataSource.sectionModels[index].model
        })
        
        dataSource.canEditRowAtIndexPath = { dataSource, indexPath in
            return true
        }
        
        return dataSource
    }
    
    private func cell(in tableView: UITableView, at indexPath: IndexPath , info: ToDoItem) -> UITableViewCell {
        
        let tableCell = tableView.dequeueReusableCell(withIdentifier: ToDoItemTableViewCell.reuseIdentifier) as? ToDoItemTableViewCell
        
        guard let cell = tableCell
            else {
                return UITableViewCell(style: .default, reuseIdentifier: "")
        }
        
        cell.set(from: info)
        
        cell.isCompletedSelect
            .asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] result in
                
                var element = info
                let index = self.changedInformationItems.firstIndex {$0.identity == element.identity}
                
                let isCompletion = result == true ? !element.isCompletion : element.isCompletion
                
                if let i = index {
                    self.changedInformationItems[Int(i)].isCompletion = isCompletion
                }
                else {
                    element.isCompletion = isCompletion
                    self.changedInformationItems.append(element)
                }
            })
            .disposed(by: cell.bag)
        
        cell.didTapped
            .asObservable()
            .skip(1)
            .subscribe(onNext: { [unowned self] in
                self.delegate?.modify(item: info, from: self)
            })
            .disposed(by: cell.bag)
        
        return cell
    }
}

extension TodoHomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in

            self.vm.removeItem(at: indexPath)
        }
        return [delete]
    }
}
