//
//  ToDoItemTableViewCell.swift
//  TestRxSwift
//
//  Created by Kyle on 2019/5/11.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class ToDoItemTableViewCell: UITableViewCell {

    static let reuseIdentifier: String = "ToDoItemTableViewCell"
    
    let isCompletedSelect: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    let didTapped: BehaviorRelay<()> = BehaviorRelay(value: ())
    
    private let scrollView: UIScrollView = UIScrollView(frame: .zero)
    
    private let scrollContentView: UIView = UIView(frame: .zero)
    
    private let btnCompleted: UIButton = {

        let btn = UIButton(type: .custom)

        btn.setImage(UIImage(named: "uncheck_circle"), for: .normal)
        btn.setImage(UIImage(named: "check_circle"), for: .selected)
        return btn
    }()
    
    private let labelTitle: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.minimumScaleFactor = 0.5
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.black
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 17)
        
        return label
    }()
    
    private let labelDate: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.minimumScaleFactor = 0.5
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 16)
        
        return label
    }()
    
    private let tapGesture = UITapGestureRecognizer()
    
    private(set) var bag: DisposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: ToDoItemTableViewCell.reuseIdentifier)
        
        self.selectionStyle = .none
        
        scrollContentView.addGestureRecognizer(tapGesture)
        configureSubviews()
        
        bindCompletionButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
        
        isCompletedSelect.accept(false)
        btnCompleted.isSelected = false

        bindCompletionButton()
    }
    
    private func bindCompletionButton() {
        isCompletedSelect
            .asDriver()
            .drive(btnCompleted.rx.isSelected)
            .disposed(by: bag)
        
        btnCompleted.rx.tap
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                self.isCompletedSelect.accept(!self.isCompletedSelect.value)
            })
            .disposed(by: bag)
        
        tapGesture.rx.event
            .asObservable()
            .subscribe(onNext: { [unowned self] event in
                
                self.didTapped.accept(())
            })
            .disposed(by: bag)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isEditing {
            showBtnSelectCompletion()
        }
        else {
            hideBtnSelectCompletion()
        }
    }
    
    private func configureSubviews() {
        
        scrollView.addSubview(btnCompleted)
        scrollContentView.addSubview(labelTitle)
        scrollContentView.addSubview(labelDate)
        
        scrollView.addSubview(scrollContentView)
        contentView.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }

        scrollContentView.snp.makeConstraints { make in
            make.edges.equalTo(scrollView)
            make.height.equalTo(scrollView.snp.height)
        }
        
        btnCompleted.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(-24)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        labelTitle.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(8)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 200, height: 40))
        }
        
        labelDate.snp.makeConstraints { make in
            make.left.equalTo(labelTitle.snp.right).offset(8)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-8)
        }
    }
    
    func set(from item: ToDoItem) {
        
        labelTitle.text = item.title
        
        if item.isCompletion {
            labelDate.text = String(format: "%@%@", arguments: ["Finsh date is", item.dueDate])
        }
        else {
            labelDate.text = String(format: "%@%@", arguments: ["Due date is ", item.startDate])
        }
    }
    
    private func showBtnSelectCompletion() {
        
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: -40, y: 0)
            self.btnCompleted.isHidden = false
        }
    }
    
    private func hideBtnSelectCompletion() {
        
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
            self.btnCompleted.isHidden = true
        }
    }
}
