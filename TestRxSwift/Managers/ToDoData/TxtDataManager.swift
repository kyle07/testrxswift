//
//  ToDoFileManager.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/9.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ToDoDataError: Error {
    
    case invalidFilePath
}

class TxtDataManager<T: Codable>: ToDoDataManagerSyncActions {
    
    typealias element = T
    
    private let dispatchQueue = DispatchQueue(label: "txt.data.manager", qos: .default, attributes: .concurrent)
    
    private let filePath: String
    
    private(set) lazy var toDoItems: BehaviorRelay<[T]> = BehaviorRelay(value: [])
    
    init(fileName: String) {
        self.filePath = String(format: "%@/%@.txt", arguments: [NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!, fileName])
    }
    
    func retriveToDoItems() -> Completable {
        
        return Completable.create { completable in
            
            guard FileManager.default.fileExists(atPath: self.filePath) else {
                completable(.error(ToDoDataError.invalidFilePath))
                return Disposables.create()
            }
            
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: self.filePath, isDirectory: false))
                let decoder = JSONDecoder()
                let list = try decoder.decode([T].self, from: data)
                
                self.toDoItems.accept(list)
                
                completable(.completed)
            }
            catch {
                completable(.error(error))
            }
            return Disposables.create()
        }
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: dispatchQueue))
    }
    
    func synchornized(_ todos: [T]) -> Completable {
        return Completable.create { completable in
            
            do {
                let data = try JSONEncoder().encode(todos)
                try data.write(to: URL(fileURLWithPath: self.filePath, isDirectory: false), options: .atomic)
                
                self.toDoItems.accept(todos)
                completable(.completed)
            }
            catch {
                completable(.error(error))
            }
            
            return Disposables.create()
        }
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: dispatchQueue))
    }
}
