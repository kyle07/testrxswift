//
//  Model.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/4/15.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation
import RxDataSources

struct ToDoItem: Codable, IdentifiableType, Equatable {
    var identity: Int
    
    var title: String
    var detail: String
    var dueDate: String
    var startDate: String
    var isCompletion: Bool
}
