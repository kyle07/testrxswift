//
//  ItemDetailViewController.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/13.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

protocol ItemDetailViewControllerDelegate: class {
    
    func confirm(from: ItemDetailViewController, item: ToDoItem)
}

class ItemDetailViewController: UIViewController {
    
    var delegate: ItemDetailViewControllerDelegate?
    
    private let textViewTitle: UITextView = {
        
        let textView = UITextView(frame: .zero)
        textView.isEditable = true
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.darkGray.cgColor
        textView.layer.cornerRadius = 10
        return textView
    }()
    
    private let textViewDetail: UITextView = {
        
        let textView = UITextView(frame: .zero)
        textView.isEditable = true
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.darkGray.cgColor
        textView.layer.cornerRadius = 10
        return textView
    }()
    
    private let switchCompleted: UISwitch = UISwitch(frame: .zero)
    
    private let stackViewCompletion: UIStackView = {
        
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    private let labelStartDate: UILabel = UILabel(frame: .zero)
    
    private let btnStartDate: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("選擇開始日", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.setTitleColor(UIColor(red: 0, green: 0, blue: 1, alpha: 0.5), for: .highlighted)
        return btn
    }()
    
    private let stackViewStartDate: UIStackView = {
        
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        return stackView
    }()
    
    private let labelDueDate: UILabel = UILabel(frame: .zero)
    
    private let btnDueDate: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("選擇目標達成日", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.setTitleColor(UIColor(red: 0, green: 0, blue: 1, alpha: 0.5), for: .highlighted)
        return btn
    }()
    
    private let stackViewDueDate: UIStackView = {
        
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        return stackView
    }()
    
    private let btnShowCompleted: UIButton = {
        
        let btn = UIButton(type: .custom)
        btn.setTitle("Confirm", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.setTitleColor(UIColor(red: 0, green: 0, blue: 1, alpha: 0.5), for: .highlighted)
        btn.backgroundColor = UIColor.green
        btn.layer.cornerRadius = 10
        return btn
    }()
    
    private let disposeBag: DisposeBag = DisposeBag()
    
    private var vm: ItemDetailViewModel
    
    private var datePickerView: UIDatePicker!
    
    init(vm: ItemDetailViewModel) {
        self.vm = vm
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        initTextViews()
        initStartDateStack()
        initDueDateStack()
        initCompletedStack()
        initButton()
        configureViews()
    }
    
    private func initTextViews() {
        
        textViewTitle.textColor = UIColor.black
        textViewDetail.textColor = UIColor.red
        
        view.addSubview(textViewTitle)
        view.addSubview(textViewDetail)
        
        // initial value
        vm.title
            .take(1)
            .subscribe(onNext: { [unowned self] text in
                
                if text == self.vm.textTitlePlaceholder {
                    self.textViewTitle.textColor = UIColor.lightGray
                }
                self.textViewTitle.text = text
            })
            .disposed(by: disposeBag)
        
        vm.detail
            .take(1)
            .subscribe(onNext: { [unowned self] text in
                
                if text == self.vm.textDetailPlaceholder {
                    self.textViewDetail.textColor = UIColor.lightGray
                }
                self.textViewDetail.text = text
            })
            .disposed(by: disposeBag)
        
        // observer
        textViewTitle.rx.text
            .asObservable()
            .skip(1)
            .map { $0!}
            .subscribe(onNext: { [unowned self] text in
                self.vm.title.accept(text)
            })
            .disposed(by: disposeBag)
        
        textViewDetail.rx.text
            .asObservable()
            .skip(1)
            .map { $0!}
            .subscribe(onNext: { [unowned self] text in
                self.vm.detail.accept(text)
            })
            .disposed(by: disposeBag)
        
        textViewTitle.rx.didBeginEditing
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                
                if self.textViewTitle.text == self.vm.textTitlePlaceholder {
                    self.textViewTitle.text = ""
                    self.textViewTitle.textColor = UIColor.black
                }
            })
            .disposed(by: disposeBag)
        
        textViewTitle.rx.didEndEditing
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                
                if self.textViewTitle.text == "" {
                    self.textViewTitle.text = self.vm.textTitlePlaceholder
                    self.textViewTitle.textColor = UIColor.lightGray
                }
            })
            .disposed(by: disposeBag)
        
        textViewDetail.rx.didBeginEditing
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                
                if self.textViewDetail.text == self.vm.textDetailPlaceholder {
                    self.textViewDetail.text = ""
                    self.textViewDetail.textColor = UIColor.red
                }
                else if self.textViewDetail.text == "" {
                    self.textViewDetail.text = self.vm.textDetailPlaceholder
                    self.textViewDetail.textColor = UIColor.lightGray
                }
            })
            .disposed(by: disposeBag)
        
        textViewDetail.rx.didEndEditing
            .asObservable()
            .subscribe(onNext: { [unowned self] in
            
                if self.textViewDetail.text == "" {
                    self.textViewDetail.text = self.vm.textDetailPlaceholder
                    self.textViewDetail.textColor = UIColor.lightGray
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func initStartDateStack() {
        
        stackViewStartDate.addArrangedSubview(labelStartDate)
        stackViewStartDate.addArrangedSubview(btnStartDate)
        view.addSubview(stackViewStartDate)

        // Initalize ui value
        vm.startDate
            .take(1)
            .bind(to: labelStartDate.rx.text)
            .disposed(by: disposeBag)
        
        // Observer
        vm.startDate
            .asDriver()
            .skip(1)
            .drive(labelStartDate.rx.text)
            .disposed(by: disposeBag)
        
        btnStartDate.rx.tap
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                self.pickDateWith(observer: self.vm.startDate)
            })
            .disposed(by: disposeBag)
    }
    
    private func initDueDateStack() {
        
        stackViewDueDate.addArrangedSubview(labelDueDate)
        stackViewDueDate.addArrangedSubview(btnDueDate)
        view.addSubview(stackViewDueDate)
        
        // Initialize ui value
        vm.dueDate
            .take(1)
            .bind(to: labelDueDate.rx.text)
            .disposed(by: disposeBag)
        
        // Observer
        vm.dueDate
            .asDriver()
            .skip(1)
            .drive(labelDueDate.rx.text)
            .disposed(by: disposeBag)
        
        btnDueDate.rx.tap
            .asObservable()
            .subscribe(onNext: { [unowned self] in
                self.pickDateWith(observer: self.vm.dueDate)
            })
            .disposed(by: disposeBag)
    }
    
    private func initCompletedStack() {
        
        let labelCompletedTitle: UILabel = UILabel(frame: .zero)
        labelCompletedTitle.textColor = UIColor.black
        labelCompletedTitle.text = "Is this Completed?"
        
        stackViewCompletion.addArrangedSubview(labelCompletedTitle)
        stackViewCompletion.addArrangedSubview(switchCompleted)
        view.addSubview(stackViewCompletion)
        
        // Initialize ui value
        vm.isCompletion
            .take(1)
            .bind(to: switchCompleted.rx.isOn)
            .disposed(by: disposeBag)
        
        // Observer
        switchCompleted.rx.isOn
            .skip(1)
            .asObservable()
            .subscribe(onNext: { [unowned self] isOn in
                self.vm.isCompletion.accept(isOn)
            })
            .disposed(by: disposeBag)
    }
    
    private func initButton() {
        
        view.addSubview(btnShowCompleted)
        
        btnShowCompleted.rx.tap
            .asObservable()
            .subscribe(onNext: { [weak self] in
                
                guard let self = self
                    else {
                        return
                }
                
                switch self.vm.manufactureItem() {
                    
                case .item(let item):
                    self.delegate?.confirm(from: self, item: item)
                case .errorMessage(let message):
                    self.showAlert(message: message)
                }
                
            }).disposed(by: disposeBag)
    }
    
    private func configureViews() {
        
        textViewTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
            make.height.equalTo(50)
        }
        
        textViewDetail.snp.makeConstraints { make in
            make.leading.equalTo(textViewTitle.snp.leading)
            make.trailing.equalTo(textViewTitle.snp.trailing)
            make.top.equalTo(textViewTitle.snp.bottom).offset(8)
            make.height.equalTo(150)
        }
        
        stackViewStartDate.snp.makeConstraints { make in
            make.leading.equalTo(textViewTitle.snp.leading)
            make.trailing.equalTo(textViewTitle.snp.trailing)
            make.top.equalTo(textViewDetail.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        stackViewDueDate.snp.makeConstraints { make in
            make.leading.equalTo(textViewTitle.snp.leading)
            make.trailing.equalTo(textViewTitle.snp.trailing)
            make.top.equalTo(stackViewStartDate.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        stackViewCompletion.snp.makeConstraints { make in
            make.leading.equalTo(textViewTitle.snp.leading)
            make.trailing.equalTo(textViewTitle.snp.trailing)
            make.top.equalTo(stackViewDueDate.snp.bottom).offset(8)
            make.height.equalTo(31)
        }
        
        btnShowCompleted.snp.makeConstraints { make in
            make.top.equalTo(stackViewCompletion.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(48)
            make.trailing.equalToSuperview().offset(-48)
            make.height.equalTo(40)
        }
    }
    
    // MARK: ViewController Utility
    private func pickDateWith(observer: BehaviorRelay<String>) {
        let alertController = UIAlertController(title: "選擇日期", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        let datePicker = UIDatePicker()
        let today: Date = Date()
        
        if let date = dateFormatter.date(from: observer.value) {
            datePicker.date = date
        }
        else {
            datePicker.date = today
        }
        
        datePicker.datePickerMode = .date
        
        alertController.view.addSubview(datePicker)
        
        let okAction = UIAlertAction(title: "確定", style: .default) { action in
            
            observer.accept(dateFormatter.string(from: datePicker.date))
        }
        
        let cancelAction = UIAlertAction(title: "取消", style: .destructive)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func showAlert(message: String) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "確定", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    deinit {
        print("ItemDetailViewController 81")
    }
}
