//
//  ItemDetailCoordinator.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/13.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit
import RxCocoa

protocol ItemDetailCoordinatorDelegate: class {
    func modify(item: ToDoItem, from coordinator: ItemDetailCoordinator)
    func add(item: ToDoItem, from coordinator: ItemDetailCoordinator)
}

enum ItemDetailType {
    
    case add
    case modify
}

class ItemDetailCoordinator: Coordinator<UINavigationController> {
    
    weak var delegate: ItemDetailCoordinatorDelegate?
    
    private var item: ToDoItem?
    private let type: ItemDetailType
    
    init(viewController: UINavigationController, item: ToDoItem? = nil, type: ItemDetailType) {
        self.type = type
        super.init(viewController: viewController)
        self.item = item
    }
    
    required init(viewController: UINavigationController) {
        fatalError("init(viewController:) has not been implemented")
    }
    
    override func start() {
        super.start()
        
        let vm = ItemDetailViewModel(item: self.item)
        let vc = ItemDetailViewController(vm: vm)
        
        switch type {
        case .add:      vc.navigationItem.title = "Add Item"
        case .modify:   vc.navigationItem.title = "Modify Item"
        }
        
        vc.delegate = self
        show(viewController: vc, animated: true)
    }
    
    deinit {
        print("ItemDetailCoordinator 81")
    }
}

extension ItemDetailCoordinator: ItemDetailViewControllerDelegate {
    
    func confirm(from: ItemDetailViewController, item: ToDoItem) {
        switch type {
        case .add:
            delegate?.add(item: item, from: self)
        case .modify:
            delegate?.modify(item: item, from: self)
        }
        rootViewController.popViewController(animated: true)
    }
}
