//
//  ItemDetailViewModel.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/13.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ManufactureItemResult {
    case item(ToDoItem)
    case errorMessage(String)
}

struct ItemDetailViewModel {
    
    var identity: Int
    let title: BehaviorRelay<String>
    let detail: BehaviorRelay<String>
    let dueDate: BehaviorRelay<String>
    let startDate: BehaviorRelay<String>
    let isCompletion: BehaviorRelay<Bool>
    
    let textTitlePlaceholder: String = "請輸入項目抬頭"
    let textDetailPlaceholder: String = "請輸入項目描述"
    
    private let utility: Utility = Utility()
    
    init(item: ToDoItem?) {
        
        if let item = item {
            identity = item.identity
            title = BehaviorRelay(value: item.title)
            detail = BehaviorRelay(value: item.detail)
            startDate = BehaviorRelay(value: item.startDate)
            dueDate = BehaviorRelay(value: item.dueDate)
            isCompletion = BehaviorRelay(value: item.isCompletion)
        }
        else {
            identity = Int(arc4random())
            title = BehaviorRelay(value: textTitlePlaceholder)
            detail = BehaviorRelay(value: textDetailPlaceholder)
            startDate = BehaviorRelay(value: "請選擇開始日")
            dueDate = BehaviorRelay(value: "請選擇目標日")
            isCompletion = BehaviorRelay(value: false)
        }
    }
    
    func manufactureItem() -> ManufactureItemResult {
        
        if title.value == "" || title.value == textTitlePlaceholder {
            return .errorMessage("請輸入項目抬頭")
        }
        
        if detail.value == "" || detail.value == textDetailPlaceholder {
            return .errorMessage("請輸入項目描述")
        }
        
        if startDate.value == "" {
            return .errorMessage("請選擇開始日")
        }
        
        if dueDate.value == "" {
            
            return .errorMessage("請選擇到期日")
        }
        
        if utility.compareDate(start: startDate.value, end: dueDate.value, dateFormat: "yyyy/MM/dd") == .orderedDescending {
            
            return .errorMessage("到期日必須是開始日或晚於開始日")
        }
        
        let item = ToDoItem(identity: identity, title: title.value, detail: detail.value, dueDate: dueDate.value, startDate: startDate.value, isCompletion: isCompletion.value)
        
        return .item(item)
    }
}
