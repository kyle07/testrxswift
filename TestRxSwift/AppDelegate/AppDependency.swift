//
//  AppDependency.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/9.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation


class AppDependency {
    
    let todoDataManager: TxtDataManager<ToDoItem> = TxtDataManager(fileName: "todo")
}

protocol CoordinatingDependency: class {
    var dependency: AppDependency? { get set}
}
