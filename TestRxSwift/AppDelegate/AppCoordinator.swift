//
//  AppCoordinator.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/9.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator<UINavigationController> {
    
    private let dependency: AppDependency = AppDependency()
    
    private var navigationDelegateProxy = NavigationDelegateProxy()
    
    override func start() {
        
        super.start()
        rootViewController.delegate = navigationDelegateProxy
        let toDoHomeCoordinator = TodoHomeCoordinator(viewController: rootViewController)
        toDoHomeCoordinator.dependency = dependency
        startChild(coordinator: toDoHomeCoordinator)
    }
}
