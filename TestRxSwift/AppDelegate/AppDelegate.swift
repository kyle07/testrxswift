//
//  AppDelegate.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/4/15.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        appCoordinator = AppCoordinator(viewController: UINavigationController())
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        window?.rootViewController = appCoordinator.rootViewController
        window?.makeKeyAndVisible()
        
        appCoordinator.start()
        
        return true
    }

}

