//
//  Utility.swift
//  TestRxSwift
//
//  Created by 浪Live_Kyle on 2019/5/14.
//  Copyright © 2019 浪Live_Kyle. All rights reserved.
//

import Foundation

struct Utility {
    
    func compareDate(start: String, end: String, dateFormat: String) -> ComparisonResult? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        guard let startDate = dateFormatter.date(from: start),
                let endDate = dateFormatter.date(from: end)
            else {
                return nil
        }
        
        return startDate.compare(endDate)
    }
}
