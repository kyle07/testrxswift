/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {

  @IBOutlet weak var imagePreview: UIImageView!
  @IBOutlet weak var buttonClear: UIButton!
  @IBOutlet weak var buttonSave: UIButton!
  @IBOutlet weak var itemAdd: UIBarButtonItem!
  
  private let bag = DisposeBag()
  private let images = BehaviorRelay<[UIImage]>(value: [])
  private var imageCache = [Int]()

  override func viewDidLoad() {
    super.viewDidLoad()

    let imagesObservable = images.share()
    
    imagesObservable
      .asObservable()
      .throttle(0.5, scheduler: MainScheduler.instance)
      .subscribe(onNext: {[weak imagePreview] (photos) in
      
        guard let preview = imagePreview else {return}
        preview.image = photos.collage(size: preview.frame.size)
      
    })
      .disposed(by: bag)
    
    imagesObservable
      .asObservable()
      .subscribe(onNext: {[weak self] (photos) in
      self?.updateUI(photos: photos)
    })
    .disposed(by: bag)
  }
  
  @IBAction func actionClear() {
    images.accept([])
    imageCache = []
  }

  @IBAction func actionSave() {

    guard let image = imagePreview.image else { return }
    PhotoWriter.save(image)
      .subscribe(onSuccess: {[weak self] id in
//        self?.showMessage("Saved with id: \(id)")
        self?.alert(title: "Saved with id: \(id)")
          .subscribe(onCompleted: {
          print("alert completed")
        }, onError: { (error) in
          print("alert error")
        })
          .disposed(by: (self?.bag)!)
        self?.actionClear()
      }) {[weak self] error in
        
        self?.alert(title: "Error", message: error.localizedDescription).subscribe(onCompleted: {
          print("alert completed")
        }, onError: { (error) in
          print("alert error")
        }).disposed(by: (self?.bag)!)
        
        self?.showMessage("Error", description: error.localizedDescription)
    }
      .disposed(by: bag)
  }

  @IBAction func actionAdd() {

//    let newImages = images.value + [UIImage(named: "IMG_1907.jpg")!]
//    images.accept(newImages)
    
    let photosViewController = storyboard!.instantiateViewController(
      withIdentifier: "PhotosViewController") as! PhotosViewController
    
    let newPhotos = photosViewController.selectedPhotos.share()

    newPhotos
      .ignoreElements()
      .subscribe(onCompleted: {[weak self] in
        self?.updateNavigationIcon()
      })
      .disposed(by: bag)
    
    newPhotos
      .takeWhile {[weak self] newImage in
        
        let count = self?.images.value.count ?? 0
        
        return count < 6
      }
      .filter{ newImage in
        return newImage.size.width > newImage.size.height
      }
      .filter { [weak self] newImage in
        let len = newImage.pngData()?.count ?? 0
        guard self?.imageCache.contains(len) == false else {
          return false
        }
        self?.imageCache.append(len)
        return true
      }
      .subscribe(onNext: {[weak self] (newImage) in
      guard let images = self?.images else { return }
      images.accept(images.value + [newImage])
    }) {
      print("completed photo selection")
    }
    .disposed(by: bag)
    
    navigationController!.pushViewController(photosViewController, animated: true)
  }

  func showMessage(_ title: String, description: String? = nil) {
    let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Close", style: .default, handler: { [weak self] _ in self?.dismiss(animated: true, completion: nil)}))
    present(alert, animated: true, completion: nil)
  }
  
  private func updateUI(photos: [UIImage]) {
    buttonSave.isEnabled = photos.count > 0 && photos.count % 2 == 0
    buttonClear.isEnabled = photos.count > 0
    itemAdd.isEnabled = photos.count < 6
    title = photos.count > 0 ? "\(photos.count) photos" : "Collage"
  }
  
  private func updateNavigationIcon() {
    let icon = imagePreview.image?
      .scaled(CGSize(width: 22, height: 22))
      .withRenderingMode(.alwaysOriginal)
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: icon,
                                                       style: .done, target: nil, action: nil)
  }
}

extension UIViewController {
  
  func alert(title: String, message: String? = nil) -> Completable {

    return Completable.create(subscribe: {[weak self] (event) -> Disposable in
      
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      alert.addAction(UIAlertAction(title: "Close", style: .default, handler: { _ in
        event(.completed)
        }
      ))
      
      self?.present(alert, animated: true, completion: nil)
      return Disposables.create {
        self?.dismiss(animated: true) {
          
          print("alert dismiss") // Won't print
        }
      }
    })
  }
}
