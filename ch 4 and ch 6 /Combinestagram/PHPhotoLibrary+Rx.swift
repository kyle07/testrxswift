//
//  PHPhotoLibrary+Rx.swift
//  Combinestagram
//
//  Created by 浪Live_Kyle on 2019/5/7.
//  Copyright © 2019 Underplot ltd. All rights reserved.
//

import Foundation
import Photos
import RxSwift

extension PHPhotoLibrary {
  
  static var authrized: Observable<Bool> {
    
    return Observable.create({ observer in
      
      DispatchQueue.main.async {
        if authorizationStatus() == .authorized {
          observer.onNext(true)
          observer.onCompleted()
        } else {
          observer.onNext(false)
          requestAuthorization { newStatus in
            observer.onNext(newStatus == .authorized)
            observer.onCompleted()
          }
        }
      }
      
      return Disposables.create()
    })
  }
}
