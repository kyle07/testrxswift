import Foundation
import RxSwift

public func example(of description: String, action: () -> Void) {
    print("\n--- Example of:", description, "---")
    action()
}

// Challenge 1 use operator 'do' let "never" to become activity
// Challenge 2 use debug() to print observable debug log
example(of: "never") {
    
    let observable = Observable<Any>.never()
    
    let disposeBag = DisposeBag()
    
    observable.do(onNext: { (element) in
//        print(element)
    }, onError: { (err) in
//        print(err)
    }, onCompleted: {
//        print("Completed")
    }, onSubscribe: {
//        print("Do Subscribe")
    }, onSubscribed: {
//        print("Do Subscribed")
    }, onDispose: {
//        print("Do dispose")
    })
    .debug("Observable")
    .subscribe(onNext: { (element) in
//        print(element)
    }, onError: { (err) in
//        print(err)
    }, onCompleted: {
//        print("Completed")
    }, onDisposed: {

    }).disposed(by: disposeBag)
}

// Challenge 2


